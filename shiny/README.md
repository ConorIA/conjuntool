# Conjuntool

A free, open source tool for the analysis of GCM data from CMIP5.

[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

_Conjuntool_ is a free, open source platform for the accessing and processing General Circulation Model output from CMIP5. It is intended for both research and pedagogy. Contributions are welcome. Visit [gitlab.com/ConorIA/conjuntool](https://gitlab.com/ConorIA/conjuntool/) to contribute. 

### Current functionality:

1. __GCM Plots__: Plot time series of GCM data, and produce monthly or annual time series for download.
2. __GCM Anomalies__: Read GCM data and generate $\Delta T$ Anomalies (change factors) over a chosen baseline.
3. __Overlay Map__: Show an overlay map of the time-period average selected data over a user-defined period.
4. __Crop Data__: Request spatial and temporal subsets of the model data (experimental)

## Why "_Conjuntool_"?:

The _Conjuntool_ (roughly: <a href="https://ipa.vpzom.click/api/synthesize/[ko%C5%8B%CB%88xuntu:l]" target="_blank">[koŋˈxuntu:l]</a>) is an open-source "re-imagining" of the UTSC Ensemble tool, a closed, limited-access platform has been used in the UTSC Climate Lab. The restricted nature of the Ensemble tool runs counter to my commitment to use only open-source software and open methodology in my research. I started writing the code that underlies this tool in Tarapoto, Peru. _Conjuntool_ is a portmanteau of "_conjunto_" (the Spanish word for "ensemble") and "tool".
