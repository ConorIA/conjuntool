On this page you can crop the GCM data from the available models. You can choose to upload an ESRI shapefile (with the necessary auxilary files) in a single ZIP archive, or you can manually choose two corners of a rectangle object. You will receive an email at the email that you provide when the data are ready for download.

Click on the map to choose coordinates. You can verify or modify the coordinates in the "Manual Coordinates" panel.

Some notes:
You should _not_ provide both a Shapefile _and_ manual input. However, if in doubt, keep the following guidelines in mind:
 - Uploading a Shapefile will reset the manual input to the default (invalid) coordinates. The Shapefile will be used.
 - If you set coordiantes manually _after_ uploading a Shapefile, we will remove the Shapefile and prefer the manual input.
 - If you change your mind again, click the map once more. When the box disappears, the Shapefile will be used.
 - If you want to use a different Shapefile, uploading it will replace any previously uploaded Shapefiles.

 
**This functionality is very fresh and is only lightly tested. There are probably a lot of bugs for which there is no code to handle them. Please report bugs to the [Conjuntool issues page](https://gitlab.com/ConorIA/conjuntool). Please also note that there is a known issue: sometimes the extent that is passed up to raster doesn't work. If that happens, try again, or try using a shapefile.**
