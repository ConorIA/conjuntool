### Conjuntool, a free, open source tool for the analysis of GCM data from CMIP5.

<b>Copyright (C) 2018&ndash;2020  Conor I. Anderson</b>

The source code for this project is available at [gitlab.com/ConorIA/conjuntool](https://gitlab.com/ConorIA/conjuntool).

_Note: The current `devel` branch will be squashed into an initial commit in a `master` branch when this hits some kind of stable status._

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

The full text of the GNU Affero General Public License is available [here](https://choosealicense.com/licenses/agpl-3.0/).

The icons in this platform are from the very cool [Font Awesome project](https://fontawesome.com/).

<p align=center><b>* * *</b></p>

#### The last 20 commits to the `devel` branch:
