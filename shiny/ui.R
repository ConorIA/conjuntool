library("shiny")
library("shinydashboard")
library("DT")
library("leaflet")
library("lubridate")
library("plotly")
library("httr")

source(dir("modules", full.names = TRUE))

r <- GET("https://gitlab.com/api/v4/projects/4693398/repository/commits",
         config = list(add_headers(accept = "application/json")))
stop_for_status(r)
commits <- jsonlite::fromJSON(rawToChar(r$content))

dashboardPage(
  dashboardHeader(title = "Conjuntool"),
  dashboardSidebar(
    sidebarMenu(
      menuItem("README", tabName = "README", icon = icon("question")),
      menuItem("GCM Plots", tabName = "plot", icon = icon("line-chart")),
      menuItem("GCM Anomalies", tabName = "anomalies", icon = icon("thermometer-three-quarters")),
      menuItem("Overlay Map", tabName = "map", icon = icon("globe")),
      menuItem("Crop Data", tabName = "crop", icon = icon("crop")),
      menuItem("Source Code", tabName = "source", icon = icon("git"))
    )
  ),
  dashboardBody(  tags$head(
    tags$style(HTML("
      .shiny-output-error-validation {
        color: gray;
       }
     "))
     ),
    tabItems(
      ## README
      tabItem(tabName = "README",
              fluidRow(
                box(width = 12,
                    includeHTML("include/fontawesome5.html"),
                    withMathJax(includeMarkdown("README.md")),
                    HTML(sprintf("<p align=\"right\">You are using <i>Conjuntool</i> on %s (commit sha: %s)",
                                 unname(Sys.info()['nodename']),
                                 commits$short_id[1]))
                )
              )
      ), # End README

      ## Tab 1
      tabItem(tabName = "plot",
              fluidRow(
                box(
                  title = "Input",
                  width = 3,
                  selectInput("var_filter_plot", "Select Variable", c("tas", "tasmax", "tasmin", "pr", "prc")),
                  checkboxInput("convert_units_plot",
                                HTML("Convert <b>K</b> to <b>&#176;C</b> OR <b>kg&#8901;m<sup>-2</sup>&#8901;s<sup>-1</sup></b> to <b>mm</b>."),
                                TRUE),
                  singleModelSelectInput("plot_model_in", "Select Model"),
                  sliderInput("year_in", label = "Projection Period", min = 2006,
                              max = 2100, value = c(2011, 2100), step = 1,
                              round = FALSE, sep = "", ticks = FALSE),
                  selectInput("period_in", "Timeframe", c("Monthly", "Annual")),
                  textInput("plot_city_in", "Location", value = "UTSC, Scarborough, ON", width = NULL, placeholder = NULL),
                  leafletOutput("plot_map", height = 200)
                ),
                box(
                  title = "Output",
                  width = 9,
                  plotlyOutput("distPlot"),
                  checkboxGroupInput("types_in", "Plot Types", list("point", "loess", "line", "bar"), inline = TRUE),
                  conditionalPanel("output.distPlot", downloadButton("download_ts", "Download time series"))
                )
              )
      ),

      # Tab 2
      tabItem(tabName = "anomalies",
              fluidRow(
                tabBox(
                  # The id lets us use input$tabset1 on the server to find the current tab
                  id = "tabset1", width = 3,
                  tabPanel("1: Def. params",
                           selectInput("var_filter", "Select Variable", c("tas", "tasmax", "tasmin", "pr", "prc")),
                           checkboxInput("convert_units",
                                         HTML("Convert <b>K</b> to <b>&#176;C</b> OR <b>kg&#8901;m<sup>-2</sup>&#8901;s<sup>-1</sup></b> to <b>mm</b>."),
                                         TRUE),
                           sliderInput("baseline_in", label = "Baseline", min = 1850,
                                       max = year(today()) - 1, value = c(1981, 2010), sep = "", ticks = FALSE),
                           HTML("<i>Model projections begin in 2005, so any baseline past that date will include climate change forcing.</i><br/><br/>"),
                           sliderInput("projection_in", label = "Projection Period", min = 2006,
                                       max = 2100, value = c(2011, 2100), sep = "", ticks = FALSE),
                           HTML("<i>The baseline can't be longer than the projections. Future periods will be defined based on the baseline length, i.e. if projection is the same length as the baseline, there will be one future period; if it is thrice as long, there will be three future periods.</i><br/><br/>"),
                           radioButtons("period_limits", "Limit Casting", choices = c("do not limit" = "none", "hindcast only" = "hind", "forecast only" = "fore"), selected = NULL,
                                        inline = FALSE, width = NULL, choiceNames = NULL, choiceValues = NULL),
                           textInput("anom_city_in", "Location", value = "UTSC, Scarborough, ON", width = NULL, placeholder = NULL),
                           leafletOutput("anom_map", height = 200)
                  ),
                  tabPanel("2: Add. filters",
                           HTML("Choose the ensembles and scenarios to analyze. Some models may not have output for all ensembles / runs.<br/><br/>"),
                           uiOutput("ScenarioFilter"),
                           uiOutput("RunFilter")
                  ),
                  tabPanel("3: Filter mods",
                           HTML("Choose the models to include in the analysis. An empty list will use ALL available models.<br/><br/>"),
                           uiOutput("ModFilter")
                  ),
                  tabPanel("4: Go!",
                           HTML("<b>Compile the Time Series</b><br/>"),
                           HTML("Press the button below to compile the time series. This can take a while.<br/><br/>"),
                           actionButton("anom_go", "Process!")
                  ),
                  tabPanel("5: Add. Opts.",
                           HTML("Use this tab to change the period of the analysis or to manipulate the final table.<br/><br/>"),
                           selectInput("anom_period_in", "Change Period of Analysis", c("Annual", "Seasonal", "Monthly")),
                           uiOutput("anom_selected_periods"),
                           checkboxGroupInput("add_proc", "Additional Processing", list("Baseline Averages", "Calculate Anomalies", "Average Ensembles/Runs", "Add Multi-Model Mean"), inline = FALSE),
                           HTML("<i>Note, some of the above options will only apply if you select multiple runs / scenarios from one model.</i> <br/><br/>")
                  )
                ),

                box(
                  width = 9,
                  status = "success",
                  conditionalPanel("$('#anoms_out').hasClass('recalculating')",
                                   HTML('<p style="font-size:20px" align="right"><i class="fas fa-circle-notch fa-spin"></i>  <b>Recalculating</b></p>')),
                  conditionalPanel("output.anoms_out", htmlOutput("anom_table_header")),
                  uiOutput("precip_anom_note"),
                  div(style = 'overflow-x: scroll', DTOutput("anoms_out")),
                  br(),
                  conditionalPanel("output.anoms_out",
                                   downloadButton("download_data", "Download this table"),
                                   downloadButton("download_anom_ts", "Download full timeseries")                  )
                )
              )
      ), # End tab 2

      # Tab 3
      tabItem(tabName = "map",
              fluidRow(
                box(
                  title = "Input",
                  width = 4,
                  selectInput("var_filter_map", "Select Variables", c("tas", "tasmax", "tasmin")),
                  singleModelSelectInput("map_model_in", "Select Model"),
                  sliderInput("year_in_2", label = "Projection Period", min = 2006,
                              max = 2100, value = c(2041, 2070), step = 1,
                              round = FALSE, sep = "", ticks = FALSE),
                  actionButton("map_go", "Map it!")
                ),
                box(
                  width = 8,
                  leafletOutput("overlay_map"),
                  br(),
                  conditionalPanel("input.map_go > 0", downloadButton("download_map", "Download this map"))
                )
              )
      ), # End tab 3

      # Tab 4
      tabItem(tabName = "crop",
              fluidRow(
                box(width = 10,
                    includeMarkdown("include/crop.md")
                ),
                box(width = 2,
                    uiOutput("latlon_checkbox"),
                    uiOutput("crop_action"),
                    conditionalPanel("input.crop_go") 
                )
              ),
              fluidRow(
                height = 7,
                tabBox(
                  # The id lets us use input$tabset1 on the server to find the current tab
                  id = "tabset2", width = 3, height = "45vh",
                  tabPanel("1: Basics",
                           selectInput("var_filter_crop", "Select Variables", c("tas", "tasmax", "tasmin", "pr")),
                           uiOutput("ScenarioFilter_crop"),
                           sliderInput("crop_year_in", label = "Years to Include", min = 1850,
                                       max = 2100, value = c(1981, 2100), step = 1,
                                       round = FALSE, sep = "", ticks = FALSE),
                           textInput("crop_email", "Enter your email to receive data")
                  ),
                  tabPanel("2: Models",
                           HTML("Choose the models from which to crop data.<br/><br/>"),
                           uiOutput("ModFilter_crop"),  style = "max-height: 40vh; overflow-y: auto;"
                  ),
                  tabPanel("3: Runs",
                           HTML("Choose ensembles / runs. Some models may not have output for all ensembles / runs.<br/><br/>"),
                           uiOutput("RunFilter_crop"),  style = "max-height: 40vh; overflow-y: auto;"
                  )
                ), 
                box(
                  width = 9,
                  leafletOutput("resample_map")
                )
              ),
              fluidRow(
                box(
                  width = 12,
                  title = "Upload a Shapefile",
                  collapsible = TRUE,
                  collapsed = FALSE,
                  fileInput("shapefile", "Choose a zip file containing .shp and other files",
                            multiple = FALSE,
                            accept = ".zip")
                )
              ),
              fluidRow(
                box(
                  width = 12,
                  title = "Manual Coordinates",
                  collapsible = TRUE,
                  collapsed = FALSE,
                  column(
                    width = 6,
                    numericInput("Lat1", "Latitide Bound 1", value = -999, width = -999),
                    numericInput("Lon1", "Longitude Bound 1", value = -999, width = -999)
                  ),
                  column(
                    width = 6,
                    numericInput("Lat2", "Latitude Bount 2", value = -999, width = -999),
                    numericInput("Lon2", "Longitude Bound 2", value = -999, width = -999)
                  )
                )
              )
      ), # End tab 4

      # Tab 5
      tabItem(tabName = "source",
              fluidRow(
                box(width = 12,
                    includeMarkdown("include/source.md"),
                    HTML(
                      paste0(
                        "<ul>",
                        paste0(
                          "<li>",
                          sprintf('<a href="https://gitlab.com/ConorIA/conjuntool/commit/%s" target="_blank">%s</a>: %s',
                                  commits$short_id, commits$short_id, gsub("\\n", "", commits$title)), collapse = "</li>"),
                        "</li></ul>")
                    )
                )
              )
      ) # End tab 5

    ) # End tabItems
  ) # End dashboardBody
) # End dashboardPage
