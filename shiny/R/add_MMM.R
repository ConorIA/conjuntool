add_MMM <- function(datin) {
  
  grouping <- c("Var", "Scenario", "Ensemble")
  if (has_name(datin, "Month")) grouping <- c(grouping, "Month")
  if (has_name(datin, "Season")) grouping <- c(grouping, "Season")
  drop <- "Model"
  if (has_name(datin, "Note")) drop <- c(drop, "Note")
  
  bind_rows(
    datin,
    dplyr::select(datin, -drop) %>%
      group_by_at(grouping) %>%
      summarize_all(mean) %>%
      add_column(Model = "MMM", .before = which(names(datin) == "Model"))
  )
}
