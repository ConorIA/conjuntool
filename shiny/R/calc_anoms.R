calc_anoms <- function(datin, var) {

  if (has_name(datin, "Ensembles")) {
    warning("It seems you already averaged the ensembles. This function needs to be run first.")
    return(datin)
  }

  dat_hist <- filter(datin, grepl("historical", Scenario))
  if (nrow(dat_hist) == 0) {
    warning("There is no historical data to calculate anomalies from.")
    return(datin)
  }

  dat_proj <- filter(datin, !grepl("historical", Scenario))

  grouping <- c("Var", "Model", "Ensemble")
  if (any(c("Month", "Season") %in% names(datin))) {
    col <- 6
    if (has_name(datin, "Month")) grouping <- c(grouping, "Month")
    if (has_name(datin, "Season")) grouping <- c(grouping, "Season")
  } else {
    col <- 5
  }

  do_calc <- function(i, dat_hist, dat_proj, grouping, col, var) {
    scenario <- if (str_detect(dat_hist$Scenario[i], pattern = "RCP[024568.]{3}")) {
      str_extract(dat_hist$Scenario[i], pattern = "RCP[024568.]{3}")
      } else {
        NULL
      }
    number_of_cols <- if (!has_name(dat_proj, "Note")) ncol(dat_proj) else ncol(dat_proj) - 1
    matched <- suppressMessages(semi_join(dat_proj, dat_hist[i,] %>% dplyr::select(grouping)))
    if (!is.null(scenario)) {
      matched <- matched %>% filter(Scenario == scenario)
      if (!has_name(matched, "Note")) matched <- add_column(matched, Note = paste("Baseline partially forced with", scenario))
    }
    if (nrow(matched) == 0) return(matched)
    matched[, col] <- unlist(dat_hist[i, col])
    for (co in (col+1):number_of_cols) {
      for (ro in 1:nrow(matched)) {
        if (var %in% c("tas", "tasmin", "tasmax")) {
          matched[ro, co] <- matched[ro, co] - matched[ro, col]
        } else {
          matched[ro, co] <- (matched[ro, co] - matched[ro, col]) / matched[ro, col] * 100
        }
      }
    }
    if (has_name(dat_hist, "Note") && length(dat_hist$Note[i] > 0)) {
      matched$Note <- dat_hist$Note[i]
    }
    matched
  }

  do.call("bind_rows", lapply(1:nrow(dat_hist), do_calc, dat_hist, dat_proj, grouping, col, var))
}
