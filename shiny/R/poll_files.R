poll_files <- function(filtered, period, coyote, scenario, ensemble) {

  data_periods <- filtered %>% group_by(Model, Scenario, Ensemble, Length) %>% summarize(Start = min(Start), End = max(End))

  out <- tibble(Model = unique(filtered$Model),
                Scenario = scenario,
                Ensemble = ensemble,
                Start = NA, End = NA, Files = NA)

  short_models <- NULL

  for (row in 1:nrow(out)) {
    good_rows <- data_periods %>% filter(Model == out$Model[row], Start <= paste0(min(period), "01"), End >= paste0(max(period), "12"))
    if (nrow(good_rows) == 0) {
      short_models <- c(short_models, out$Model[row])
    } else {
      if (nrow(good_rows) > 1) good_rows <- good_rows[which(good_rows$Length == max(good_rows$Length)),]
      model_files <- filtered %>% filter(Model == good_rows$Model, Scenario == good_rows$Scenario, Ensemble == good_rows$Ensemble, Length == good_rows$Length) %>% dplyr::select(Start, End, Filenames)
      out$Start[row] <- min(model_files$Start)
      out$End[row] <- max(model_files$End)
      out$Files[row] <- list(model_files$Filenames[max(which(model_files$Start <= paste0(min(period), "01"))):min(which(model_files$End >= paste0(max(period), "12")))])
    }
  }

  if (length(short_models) > 0) {
    for (mod in short_models) {
      model_stats <- data_periods %>% filter(Model == mod)
      # Start with out start date
      start_row <- max(which(model_stats$Start <= paste0(min(period), "01")))
      if (length(start_row) == 0) {
        stop("No start!")
      } else {
        model_files <- filtered %>% filter(Model == mod, Scenario == model_stats$Scenario[start_row], Ensemble == model_stats$Ensemble[start_row], Length == model_stats$Length[start_row]) %>% dplyr::select(Start, End, Filenames)
        next_row <- suppressWarnings(try(min(which(as.yearmon(model_stats$Start, format = "%Y%m") == (as.yearmon(model_stats$End[start_row], format = "%Y%m") + 1/12) | (model_stats$Start <= model_stats$End[start_row] & model_stats$End > model_stats$End[start_row]))), silent = TRUE))
        while (next_row != Inf) {
          if (model_stats$End[next_row] >= paste0(max(period), "12")) {
            model_files <- bind_rows(model_files, filtered %>% filter(Model == mod, Scenario == model_stats$Scenario[next_row], Ensemble == model_stats$Ensemble[next_row], Length == model_stats$Length[next_row]) %>% dplyr::select(Start, End, Filenames))
            break
          } else {
            model_files <- bind_rows(model_files, filtered %>% filter(Model == mod, Scenario == model_stats$Scenario[next_row], Ensemble == model_stats$Ensemble[next_row], Length == model_stats$Length[next_row]) %>% dplyr::select(Start, End, Filenames))
            next_row <- suppressWarnings(try(min(which(as.yearmon(model_stats$Start, format = "%Y%m") == (as.yearmon(model_stats$End[next_row], format = "%Y%m") + 1/12) | (model_stats$Start <= model_stats$End[next_row] & model_stats$End > model_stats$End[next_row]))), silent = TRUE))
          }
        }
        if (next_row == Inf) {
          message(paste("Not enough data for", mod))
        } else {
          out$Start[out$Model == mod] <- model_files$Start[max(which(model_files$Start <= paste0(min(period), "01")))]
          out$End[out$Model == mod] <- model_files$End[min(which(model_files$End >= paste0(max(period), "12")))]
          out$Files[out$Model == mod] <- list(c(model_files$Filenames[max(which(model_files$Start <= paste0(min(period), "01"))):min(which(model_files$End >= paste0(max(period), "12")))]))
          short_models <- short_models[-which(short_models == mod)]
        }
      }
    }
  }

  if (coyote) return (list(Table = out, Shorts = short_models))
  return(out)
}